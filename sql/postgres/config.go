package postgres

import (
	"fmt"

	_ "github.com/lib/pq" // postgres connection lib
)

type Config struct {
	Host     string
	Port     string
	User     string
	Password string
	Name     string
}

func (c Config) GetDataSource() string {
	return fmt.Sprintf(
		"user=%s password=%s dbname=%s host=%s port=%s sslmode=disable",
		c.User, c.Password, c.Name, c.Host, c.Port,
	)
}

func (c Config) GetDriverName() string {
	return "postgres"
}
