-include .local
export

.PHONY: gci
gci:
	@gofumpt -w -extra .
	@gci write --skip-vendor --skip-generated -s standard -s default -s "prefix(gitlab.com/golib3/core)" --custom-order .

.PHONY: lint
lint:
	@golangci-lint run

.PHONY: test
test:
	@go test ./...

.PHONY: generate
generate:
	@buf generate
