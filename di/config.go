package di

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
)

const defaultHTTPPort = "80"

const AppEnvDev = "dev"

type Config struct {
	configStorage *viper.Viper
	serviceName   string
	appEnv        string
	httpPort      string
}

func NewConfig() (*Config, error) {
	v := viper.New()
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	v.AutomaticEnv()

	v.SetDefault("http.port", defaultHTTPPort)
	v.SetDefault("app.env", AppEnvDev)
	v.SetConfigName("config")
	v.SetConfigType("yaml")
	v.AddConfigPath(v.GetString("app.config.path"))

	if err := v.ReadInConfig(); err != nil {
		return nil, fmt.Errorf("error reading config file: %w", err)
	}

	return &Config{
		configStorage: v,
		serviceName:   v.GetString("app.service.name"),
		appEnv:        v.GetString("app.env"),
		httpPort:      v.GetString("http.port"),
	}, nil
}

func (c *Config) GetAppEnv() string {
	return c.appEnv
}

func (c *Config) GetServiceName() string {
	return c.serviceName
}

func (c *Config) GetHTTPPort() string {
	return c.httpPort
}

func (c *Config) GetStorage() *viper.Viper {
	return c.configStorage
}
